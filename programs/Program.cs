﻿using System;
using System.Linq;

namespace Sample
{
    class Program
    {
        static int Min(int[] d, int n, int[] f)
        {
            int minv = 999, pos = 0;
            for (int i = 0; i < n; i++)
            {
                if (f[i] != 1 && minv > d[i])
                {
                    minv = d[i];
                    pos = i;
                }
            }
            f[pos] = 1;
            return pos;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Enter no of Nodes(Nodes start from 1)");
            int n = int.Parse(Console.ReadLine());
            int[,] w = new int[n, n];
            int[] d = Enumerable.Repeat(999, n).ToArray();
            int[] f = new int[n];
            int[] sp = new int[n];
            int i, j, t;
            for (i = 0; i < n; i++)
                for (j = 0; j < n; j++)
                    w[i, j] = 999;
            Console.WriteLine("Enter point1 , Point2 and Distance between them");
            while (true)
            {
                i = int.Parse(Console.ReadLine());
                if (i == -1) break;
                j = int.Parse(Console.ReadLine());
                w[i - 1, j - 1] = int.Parse(Console.ReadLine());
                w[j - 1, i - 1] = w[i - 1, j - 1];
                Console.WriteLine("Ditance between point{0} and point{1} is {2}.", i, j, w[i - 1, j - 1]);
                Console.WriteLine("Enter -1 to exit");
            }
            Console.WriteLine("Enter Source and Destination");
            int s = int.Parse(Console.ReadLine()) - 1;
            int source = s;
            int des = int.Parse(Console.ReadLine()) - 1;
            f[s] = 1;
            d[s] = 0;
            for (i = 0; i < n; i++)
            {
                for (j = 0; j < n; j++)
                    Console.Write(w[i, j] + "  ");
                Console.WriteLine();
            }
            t = n;
            while (t - 1 != 0)
            {
                for (i = 0; i < n; i++)
                {
                    if (f[i] != 1 && w[s, i] != 999)
                    {
                        if (d[i] > d[s] + w[s, i])
                        {
                            sp[i] = s;
                            d[i] = d[s] + w[s, i];
                        }
                    }
                    //Console.Write(d[i] + " ");
                }
                Console.WriteLine();
                s = Min(d, n, f);
                t--;
            }
            //for (i = 0; i < n; i++)
            //{
            //    Console.Write(d[i] + " ");
            //}
            //Console.WriteLine();
            int temp = des;
            //for (i = 0; i < n; i++)
            //{
            //    Console.Write(sp[i] + " ");
            //}
            int[] result = new int[5];
            for (i = 0; i < n; i++)
            {
                result[i] = sp[temp] + 1;
                if (sp[temp] == source)
                    break;
                temp = sp[temp];
            }
            Console.WriteLine();
            Console.Write("The Shortest path between source {0} and Destination {1} is ", (source + 1), (des + 1));
            for (int k = i; k >= 0; k--)
                Console.Write(result[k] + "->");
            Console.Write(des + 1);
            Console.Write(" with distance " + d[des]);
            Console.ReadKey();
        }
    }
}
